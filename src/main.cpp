#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdexcept>
#include <iostream>
#include <ctime>

#include <eo>
#include <ga.h>

using namespace std;

// 一、定义染色体编码
typedef eoBit<double> Indi;  // 二进制编码,适应值为浮点数

// 自定义问题类.
// 该类存储专业功能相关的数据以及计算适应值!
class Problem
{
    public:
        // 构造函数
        Problem()
        {
            // 人为指定目标最优个体(10位, 编码: 0111000011)
            // 注: 目前这么写仅作测试用!!!
            result_indi.push_back(0);
            result_indi.push_back(1);
            result_indi.push_back(1);
            result_indi.push_back(0);
            result_indi.push_back(0);
            result_indi.push_back(1);
            result_indi.push_back(0);
            result_indi.push_back(1);
            result_indi.push_back(0);
            result_indi.push_back(1);
        }
        ~Problem() {}
        // 计算适应值(真正的适应值计算在这里!)
        double fitness(const std::vector<bool>& indi)
        {
            // 适应值计算思路:
            //     将2个染色体的每一位相减,再绝对值求和,最后取负值,作为染色体indi的适应值
            // 例如:
            //     X = (0,1,1,0,0),   Y = (0,0,0,1,1)
            //     适应值f = 10 - ∑|Xi-Yi|     (f的取值为[0,10],f值越大表示X和Y越接近)
            double sum = 0;
            for(unsigned int i=0;i<indi.size();i++)
            {
                int x1 = indi[i], x2 = result_indi[i];
                sum += abs(x1-x2);
            }
            return 10-sum;   // eo库默认的是求最大问题，适应值为负值则等价于求最小问题
        }
        //辅助成员函数 -- 将vector的数据填充到染色体对象中并计算适应值
        void toIndi(Indi& _indi)
        {
            _indi.value(result_indi);
            _indi.fitness(this->fitness(_indi));
        }
    // 私有成员变量(具体要定义哪些变量视要求解的问题而定!)
    private:
        // 代表已知的最优个体
        std::vector<bool> result_indi;
};

// 二、自定义适应值函数
// (2) 采用C++仿函数类作为适应值函数, 可以传入额外的参数,个数不受限制
//     注: 仿函数是C++中的一种比较高级的技巧,可以像是用函数一样使用一个C++类对象!
class MyEvalFunc : public eoEvalFunc<Indi>
{
    public:
        MyEvalFunc(Problem& _problem) : problem(_problem) { }
        // 必须要重载()运算符,eo库调用该重载函数进行适应值计算!
        void operator()(Indi& _indi)
        {
            if(_indi.invalid())
            {
                // 说明: 真正的适应值计算在Problem类中,这样可以将遗传算法代码与专业问题分离开!
                _indi.fitness(problem.fitness(_indi));
            }
        }
    private:
        Problem& problem;
};

// 主函数入口
void main_function(int argc, char **argv)
{
    // 三、设置遗传算法参数
    const unsigned int SEED = time(0);    // 以当前时间作为随机数种子
    const unsigned int VEC_SIZE = 10;     // 染色体编码长度
    const unsigned int POP_SIZE = 100;    // 种群大小(或规模)
    const unsigned int MAX_GEN = 400;     // 最大进化代数
    const unsigned int T_SIZE = 3;        // 竞赛选择算子的参数
    const float CROSS_RATE = 0.8;         // 交叉率
    const double P_MUT_PER_BIT = 0.01;    // 采用单点变异算子,每一位发生变异的概率
    const float MUT_RATE = 1.0;           // 变异率(每个染色体发生变异的概率)

    // 四、设置随机数种子(必须进行设置,否则遗传算法的结果始终不变!)
    rng.reseed(SEED);

    // 五、定义适应值算子对象
    // (1) 与专业相关的问题模型(自定义类)
    Problem problem;
    // (2) 自定义适应值函数类,用户可以额外的传入1个新的参数
    //     注: 大多数专业计算需要传递很多的参数,光给定一个染色体编码是无法进行计算的!
    MyEvalFunc eval(problem);

    // 构造原始的真实解对应的染色体(便于后续打印输出用!)
    Indi result_indi;
    problem.toIndi(result_indi);

    // 六、初始化种群
    //(1) 定义一个空的种群
    // 注:eoPop表示种群类(这是一个C++模板类,等于一个数组,里面存放都是染色体类型(Indi)的对象),
    //    Indi表示染色体编码的类型(本例使用二进制编码,参见第18行的typedef定义)
    eoPop<Indi> pop;
    //(2) 初始化种群(循环100次,向种群中增加100个染色体编码)
    for (unsigned int i=0; i<POP_SIZE; i++)
    {
        //(3) 定义一个新的染色体(等价于一个空的数组)
        Indi v;
        //(4) 随机生成染色体编码中的每一位！
        for (unsigned j=0; j<VEC_SIZE; j++)
        {
            bool r = rng.flip(); // 生成随机数: 0或1
            v.push_back(r);      // 确定该染色体第j个位置的二进制数值
        }
        //(5) 计算染色体的适应值(内部调用我们自定义的适应值函数,并将适应值保存在染色体对象内部!)
        eval(v);
        //(6) 添加新生成的染色体到初始种群
        pop.push_back(v);
    }
    // Print (sorted) intial population (raw printout)
    cout << "初始种群:" << endl;
    cout << pop;

    // 七、定义相关的算子
    // (1) 定义选择算子 -- 采用"竞赛选择方法"
    eoDetTournamentSelect<Indi> select(T_SIZE);
    // 定义选择算子 -- 采用"轮盘赌选择方法"(该算子有个限制: 适应值不能为负值或0,否则可能运行会出错!)
    //eoProportionalSelect<Indi> select;
    // (2) 定义交叉算子 -- 采用"二进制编码的单点交叉"方法
    eo1PtBitXover<Indi> xover;
    // (3) 定义变异算子 -- 采用"二进制编码的单点变异"方法
    eoBitMutation<Indi>  mutation(P_MUT_PER_BIT);

    // 八、定义终止条件
    // 遗传算法进化MAX_GEN代之后停止计算
    eoGenContinue<Indi> continuator(MAX_GEN);

    // 九、标准遗传算法SGA
    // 将选择算子、交叉算子、交叉率、变异算子、变异率、适应值函数，终止条件作为参数输入
    eoSGA<Indi> gga(select,                  // 选择算子
                    xover, CROSS_RATE,       // 交叉算子和交叉率
                    mutation, MUT_RATE,      // 变异算子和变异率
                    eval,                    // 适应值函数算子
                    continuator);            // 终止条件

    // 十、从初始种群开始执行进化算法直到满足终止条件结束
    // 注：这是C++运算符重载后的效果,使一个C++类对象可以像一个函数那样使用.
    gga(pop);

    // 十一、最后得到的种群按照适应值排序,并打印输出种群信息
    pop.sort();
    //pop.sortedPrintOn(cout);
    cout << "最终的种群:\n" << pop << endl;

    // 打印原始的真实解(原始编码:0111000011)
    cout<< "真实解对应染色体:" << result_indi << endl;
    // 打印遗传算法找出最优个体(染色体编码: 0111000011)
    cout<< "遗传算法最优个体:" << pop.best_element() << endl;
}

 // 主函数
int main(int argc, char **argv)
{
    try
    {
        // 主函数入口
        // 注1: 如果该函数运行出现异常,则程序会跳转到catch语句中,给予开发人员了解程序异常原因的机会.
        // 注2: try/catch是标准C++中的语法,可以避免程序异常崩溃!
        main_function(argc, argv);
    }
    catch(exception& e)
    {
        cout << "异常原因: " << e.what() << '\n';
    }

    return 0;
}
