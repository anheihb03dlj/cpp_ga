# cpp_ga

## 一、目前找了个2个C++的遗传算法库，放在了ThirdParty目录下
* 1、GALib
* 2、ParadisEO
这2个库具体的编译方法在各自的文件夹中！

## 二、遗传算法测试项目ga_test
* 1、该项目使用遗传算法库ParadisEO，需要在codeblocks+mingw下编译运行，与LBM开源库Palabos的开发环境一致！
     注：使用codeblocks打开ga_test/ga_test.cbp项目文件!
* 2、如果要运行编译和运行该项目，请搭建mingw开发环境
+ （1）安装codeblocks+mingw编译器
+ （2）安装cmake（可选）
如果想自己编译ParadisEO的源代码就需要安装，如果仅仅是使用ParadisEO库，不建议折腾!
+ （3）本压缩包提供的安装程序
mingw开发环境
       |-- [codeblocks-16.01mingw-setup.exe](http://www.codeblocks.org/downloads/26)   （codeblocks+mingw编译器）
       |-- cmake

## 三、测试项目的代码说明
具体的遗传算法代码参见ga_test/main.cpp，里面有详细的中文注释!!!

## 四、问题模型
参见《问题模型和遗传算法.docx》

## 五、其它资料
* 1、《遗传算法库选择.doc》      
* 2、《遗传算法库(ParadisEO)文档.doc》
       如果要深入了解ParadisEO，请仔细阅读该文档！
